//package ee.vacation.bean;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Boss extends Employee{
//	
//	List<Employee> employeeList = new ArrayList<>();
//	
//	public List<Employee> getEmployeeList() {
//		return employeeList;
//	}
//
//	public Boss(String name, String userName, long idCode, String department, String workBeginningDate) {
//		super(name, userName, idCode, department, workBeginningDate);
//	}
//	
//	public void confirmVacation(Vacation vacation) {
//		vacation.setVacationStatus("confirmed");
//	}
//	
//	public void rejectVacation(Vacation vacation) {
//		vacation.setVacationStatus("rejected");
//	}
//	
//	/**
//	 * method for the boss object to create a new employee object
//	 * @param name
//	 * @param userName
//	 * @param password
//	 * @param idCode
//	 * @param workBeginningDate
//	 */
//	public void createEmployee(String name, String userName, String password, long idCode, String workBeginningDate) {
//		Employee employee = new Employee(name, userName, idCode, this.getDepartment(), workBeginningDate);
//		employee.setPassword(password);
//		employeeList.add(employee);
//	}
//	
//}
