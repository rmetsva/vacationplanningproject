package ee.vacation.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ee.vacation.service.EmployeeService;

public class Vacation {
	private String vacationBeginning;
	private String vacationEnd;
	private String vacationType;
	private String vacationStatus;
	private int employeeId;
	private int vacationId;
	private int vacationLength;


	public Vacation() {
	}

	public Vacation (String vacationBeginning, String vacationEnd, String vacationType) {
		this.vacationBeginning = vacationBeginning;
		this.vacationEnd = vacationEnd;
		this.vacationType = vacationType;
		this.vacationLength = this.lengthOfVacation();
	}
	
	public Vacation (String vacationBeginning, String vacationEnd, String vacationType, String vacationStatus) {
		this.vacationBeginning = vacationBeginning;
		this.vacationEnd = vacationEnd;
		this.vacationType = vacationType;
		this.vacationStatus = vacationStatus;
		this.vacationLength = this.lengthOfVacation();
	}
	
	public Vacation (int vacationId, String vacationBeginning, String vacationEnd, String vacationType, String vacationStatus, int employeeId) {
		this.vacationBeginning = vacationBeginning;
		this.vacationEnd = vacationEnd;
		this.vacationType = vacationType;
		this.vacationStatus = vacationStatus;
		this.employeeId = employeeId;
		this.vacationId = vacationId;
		this.vacationLength = this.lengthOfVacation();
	}
	
	
	public String getVacationBeginning() {
		return vacationBeginning;
	}


	public String getVacationStatus() {
		return vacationStatus;
	}
	
	public String getVacationEnd() {
		return vacationEnd;
	}

	public String getVacationType() {
		return vacationType;
	}

	public void setVacationStatus(String vacationStatus) {
		this.vacationStatus = vacationStatus;
	}
	
	public int getEmployeeId() {
		return employeeId;
	}

//	public void setEmployeeId(int employeeId) {
//		this.employeeId = employeeId;
//	}
	
	public int getVacationId() {
		return vacationId;
	}

	public int getVacationLength() {
		return vacationLength;
	}


	/**
	 * method that makes String into Date
	 * @param string
	 * @return
	 */
	public Date stringToDate(String string) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = new Date();
		try {
			date = format.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	/**
	 * method that calculates the length of the vacation
	 * @return
	 */
	public int lengthOfVacation() {
		Date beginningDate = stringToDate(vacationBeginning);
		Date endDate = stringToDate(vacationEnd);
		
		return Math.toIntExact((endDate.getTime() - beginningDate.getTime())/86400000 + 1);
	}
	
	
	/**
	 * method that check if the vacation created by the employee is valid (starts at least 7 days from the day inserted; ends at the same day or later and is shorter or equal to 
	 * the amount of vacation days left - knows the difference between regular and bonus vacation days; starts no later than 500 days from the day created)
	 * @return
	 */
	public boolean particularVacationValid() {
		Date beginningDate = stringToDate(this.vacationBeginning);
		Date endDate = stringToDate(this.vacationEnd);
		Date dateNow = new Date();
		int howFarBeginning = Math.toIntExact((beginningDate.getTime() - dateNow.getTime())/86400000);
		
		int howFarEndFromBeginning = Math.toIntExact((endDate.getTime() - beginningDate.getTime())/86400000 + 1);
		
		
		EmployeeService employeeService = new EmployeeService();
		
		int arg = 0;
		
		if (this.vacationType.equals("regular")) {
			arg = employeeService.getEmployeeById(this.employeeId).get(0).vacationDaysLeft();
		} else if (this.vacationType.equals("bonus")) {
			arg = employeeService.getEmployeeById(this.employeeId).get(0).bonusVacationDaysLeft();
		}
		
		
		if (howFarBeginning >= 7 && howFarBeginning <= 500 && howFarEndFromBeginning >= 1 && howFarEndFromBeginning <= arg) {
			return true;
		} else {
			return false;
		}
	}

}
