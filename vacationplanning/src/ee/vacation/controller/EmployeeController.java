package ee.vacation.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ee.vacation.bean.Employee;
import ee.vacation.service.EmployeeService;

@Path("/employees")
public class EmployeeController {
	
	EmployeeService employeeService = new EmployeeService();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployees() {
		List<Employee> listOfEmployees = employeeService.getAllEmployees();
		return listOfEmployees;
	}
	
	@GET
	@Path("/query")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> checkEmployee(@QueryParam("username") String username, @QueryParam("password") String password) {
		return employeeService.checkEmployee(username, password);
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployeeById(@PathParam("id") int employeeId) {
		List<Employee> listOfEmployees = employeeService.getEmployeeById(employeeId);
		return listOfEmployees;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Employee addEmployee(Employee employee) {
		return employeeService.addEmployee(employee);
	}


}
