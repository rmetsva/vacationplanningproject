package ee.vacation.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ee.vacation.bean.Vacation;
import ee.vacation.service.VacationService;

@Path("/vacations")
public class VacationController {

	VacationService vacationService = new VacationService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Vacation> getVacations() {
		List<Vacation> listOfVacations = vacationService.getAllVacations();
		return listOfVacations;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Vacation> getVacationByEmployeeId(@PathParam("id") int employeeId) {
		return vacationService.getVacationsById(employeeId);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Vacation addVacation(Vacation vacation) {
		return vacationService.addVacation(vacation);
	}
	
	@GET
	@Path("/query")
	@Produces(MediaType.APPLICATION_JSON)
	public void updateVacation(@QueryParam("id") int id, @QueryParam("status") String status) {
		vacationService.updateVacation(id, status);
	}
	
	@GET
	@Path("/query2")
	@Produces(MediaType.APPLICATION_JSON)
	public void updateVacationDates(@QueryParam("id") int id, @QueryParam("beginning") String beginning, @QueryParam("end") String end) {
		vacationService.updateVacationDates(id, beginning, end);
	}
	
	
}
