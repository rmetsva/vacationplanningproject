package ee.vacation.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionHandling {
	
	public static Connection createConnection() {
		Connection connection = null;
		loadDriver();
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost/vacation_planning?" + "user=raul&password=vacation4");
		} catch (SQLException e) {
			System.out.println("Creating database connection has failed");
			e.printStackTrace();
		}
		return connection;
	}
	
	private static void loadDriver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			System.out.println("Loading MySQL driver has failed");
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Closing database connection has failed");
			e.printStackTrace();
		}
	}
}
