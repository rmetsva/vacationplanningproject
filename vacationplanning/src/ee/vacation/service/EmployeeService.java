package ee.vacation.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.api.jdbc.Statement;

import ee.vacation.bean.Employee;

public class EmployeeService {
	public List<Employee> getAllEmployees() {
		List<Employee> listOfEmployees = new ArrayList<>();

		Connection connection = DBConnectionHandling.createConnection();

		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM employees;");
			while (resultSet.next()) {
				Employee employee = new Employee(resultSet.getInt("employee_id"), resultSet.getString("name"), resultSet.getString("username"), resultSet.getLong("id_code"), resultSet.getString("department"), resultSet.getString("work_beginning_date"));
				listOfEmployees.add(employee);
			}
		} catch (SQLException e) {
			System.out.println("Reading data from database has failed");
			e.printStackTrace();
		}
		
		DBConnectionHandling.closeConnection(connection);
		return listOfEmployees;
	}

	public List<Employee> checkEmployee(String username, String password) {
		List<Employee> listOfEmployees = new ArrayList<>();
		Connection connection = DBConnectionHandling.createConnection();
		
		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM employees WHERE username='" + username + "' AND password='" + password + "';");
			
			while (resultSet.next()) {
					Employee employee = new Employee(resultSet.getInt("employee_id"), resultSet.getString("name"), resultSet.getString("username"), resultSet.getLong("id_code"), resultSet.getString("department"), resultSet.getString("work_beginning_date"));
					listOfEmployees.add(employee);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		DBConnectionHandling.closeConnection(connection);
		return listOfEmployees;
	}
	
	public Employee addEmployee(Employee employee) {
		
		if(employee.particularEmployeeValid()) {
		
		Connection connection = DBConnectionHandling.createConnection();
		
		try (Statement statement = (Statement) connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employees (name, username, password, id_code, department, work_beginning_date, boss_id) VALUES ('" + employee.getName() + "', '" + employee.getUserName() + "', '"
					+ employee.getPassword() + "', " + employee.getIdCode() + ", '" + employee.getDepartment() + "', '" + employee.getWorkBeginningDate() + "', " + employee.getBossId() + ");");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		
		}
		
		return employee;
	}
	
	public List<Employee> getEmployeeById(int employeeId) {
		List<Employee> listOfEmployees = new ArrayList<>();

		Connection connection = DBConnectionHandling.createConnection();

		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM employees WHERE employee_id=" + employeeId + ";");
			while (resultSet.next()) {
				
				Employee employee = new Employee(resultSet.getInt("employee_id"), resultSet.getString("name"), resultSet.getString("username"), resultSet.getLong("id_code"), resultSet.getString("department"), resultSet.getString("work_beginning_date"));
				listOfEmployees.add(employee);
				
			}
		} catch (SQLException e) {
			System.out.println("Reading data from database has failed");
			e.printStackTrace();
		}
		
		DBConnectionHandling.closeConnection(connection);
		return listOfEmployees;
	}
	
}