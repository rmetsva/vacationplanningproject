package test.ee.vacation;

import org.junit.Assert;
import org.junit.Test;

import ee.vacation.bean.Employee;
import ee.vacation.bean.Vacation;

public class EmployeeTest {
	
	@Test
	public void testIfVacationDaysLeftWorks() {
		Employee testEmployee1 = new Employee(1,"Mati Tamm", "matitamm", 37608192010l, "finance", "2016-05-05");
		Employee testEmployee2 = new Employee(2, "Kati Tamm", "katitamm", 47608192010l, "finance", "2011-05-05");
		Assert.assertEquals(46, testEmployee1.vacationDaysLeft());
		Assert.assertEquals(56, testEmployee2.vacationDaysLeft());
	}
	
	@Test
	public void testIfLengthOfVacationWorks() {
		Vacation testVacation1 = new Vacation("2016-02-25", "2016-03-05", "regular");
		Assert.assertEquals(10, testVacation1.lengthOfVacation());
	}
	
	@Test
	public void testIfBonusVacationDaysLeftWorks() {
		Employee testEmployee = new Employee(1, "Mati Tamm", "matitamm", 37608192010l, "finance", "2013-02-01");
		Assert.assertEquals(4, testEmployee.bonusVacationDaysLeft());
		
	}
}
