package ee.vacation.bean;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import ee.vacation.service.VacationService;

public class Employee {
	private String name;
	private String userName;
	private String password;
	private long idCode;
	private String department;
	private String workBeginningDate;
	//List<Vacation> vacationList = new ArrayList<>();
	private int vacationDaysLeft;
	private int bonusVacationDaysLeft;
	private int employeeId;
	private int bossId;

	public Employee() {
	}

	public Employee(String name, String userName, long idCode, String department, String workBeginningDate) {
		this.name = name;
		this.userName = userName;
		this.idCode = idCode;
		this.department = department;
		this.workBeginningDate = workBeginningDate;
		this.vacationDaysLeft = this.vacationDaysLeft();
		this.bonusVacationDaysLeft = this.bonusVacationDaysLeft();
	}

	public Employee(int employeeId, String name, String userName, long idCode, String department,
			String workBeginningDate) {
		this.name = name;
		this.userName = userName;
		this.idCode = idCode;
		this.department = department;
		this.employeeId = employeeId;
		this.workBeginningDate = workBeginningDate;
		this.vacationDaysLeft = this.vacationDaysLeft();
		this.bonusVacationDaysLeft = this.bonusVacationDaysLeft();
	}

	public String getPassword() {
		return password;
	}

//	public void setPassword(String password) {
//		this.password = password;
//	}

	public String getName() {
		return name;
	}

//	public void setName(String name) {
//		this.name = name;
//	}

	public String getDepartment() {
		return department;
	}

//	public void setDepartment(String department) {
//		this.department = department;
//	}

	public String getUserName() {
		return userName;
	}

	public long getIdCode() {
		return idCode;
	}

	public String getWorkBeginningDate() {
		return workBeginningDate;
	}

	public int getVacationDaysLeft() {
		return vacationDaysLeft;
	}

	public int getBonusVacationDaysLeft() {
		return bonusVacationDaysLeft;
	}

	public int getEmployeeId() {
		return employeeId;
	}

//	public void setEmployeeId(int employeeId) {
//		this.employeeId = employeeId;
//	}

	public int getBossId() {
		return bossId;
	}

	/**
	 * 
	 * method that makes String into Date
	 */
	public Date stringToDate() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = new Date();
		try {
			date = format.parse(this.workBeginningDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * method to calculate the vacation days that are left for a particular
	 * employee
	 */
	public int vacationDaysLeft() {
		Date dateOfFirstDay = stringToDate();

		Calendar cal = Calendar.getInstance();
		cal.setTime(dateOfFirstDay);
		int year1 = cal.get(Calendar.YEAR);

		Date lastDayOfFirstYear = new GregorianCalendar(year1, Calendar.DECEMBER, 31).getTime();

		int daysUntilEndOfYear = Math.toIntExact((lastDayOfFirstYear.getTime() - dateOfFirstDay.getTime()) / 86400000);

		int firstYearVacationDays = (int) (Math.round((double) (daysUntilEndOfYear * 28) / 365));

		Date dateNow = new Date();

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(dateNow);
		int yearNow = cal1.get(Calendar.YEAR);

		int allVacationDays;

		if (year1 >= yearNow - 1) {
			allVacationDays = firstYearVacationDays + (28 * (yearNow - year1));
		} else {
			allVacationDays = 56;
		}
		int lengthOfAllRegularVacations = 0;
		
		VacationService vacationService = new VacationService();
		
		lengthOfAllRegularVacations = vacationService.calculateLengthOfAllVacations(this.getEmployeeId(), "regular");
		
		return allVacationDays - lengthOfAllRegularVacations;
		
		
	}

	/**
	 * Method that calculates the bonus vacation days left for an employee
	 * 
	 * @return
	 */
	public int bonusVacationDaysLeft() {
		Date dateOfFirstDay = stringToDate();

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(dateOfFirstDay);
		int year1 = cal1.get(Calendar.YEAR);
		int month1 = cal1.get(Calendar.MONTH);
		int day1 = cal1.get(Calendar.DAY_OF_MONTH);

		Date dateNow = new Date();

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(dateNow);
		int yearNow = cal2.get(Calendar.YEAR);
		int monthNow = cal2.get(Calendar.MONTH);
		int dayNow = cal2.get(Calendar.DAY_OF_MONTH);
		
		int allVacationDays;
		
		if (yearNow <= year1) {
			allVacationDays = 0;
		} else if ((monthNow > month1 || monthNow == month1 && dayNow > day1) && ((yearNow - year1) <= 5)) {
			allVacationDays = yearNow - year1;
		} else if ((monthNow > month1 || monthNow == month1 && dayNow > day1) && ((yearNow - year1) > 5)) {
			allVacationDays = 5;
		} else if ((monthNow < month1 || monthNow == month1 && dayNow <= day1) && ((yearNow - year1) <= 5)) {
			allVacationDays = yearNow - year1 - 1;
		} else {
			allVacationDays = 5;
		}
		int lengthOfAllBonusVacations = 0;
		
		VacationService vacationService = new VacationService();
		
		lengthOfAllBonusVacations = vacationService.calculateLengthOfAllVacations(this.getEmployeeId(), "bonus");
		
		return allVacationDays - lengthOfAllBonusVacations;
	}

	/**
	 * method for creating a Vacation object
	 * 
	 * @param beginning
	 * @param end
	 * @param type
	 * @param status
	 */
//	public void createVacation(String beginning, String end, String type, String status) {
//		Vacation vacation = new Vacation(beginning, end, type);
//		vacationList.add(vacation);
//	}
	
	
	/**
	 * method that check if the new employee inserted has a password with a sufficient length and an id code with a correct length
	 * @return
	 */
	public boolean particularEmployeeValid() {
		
		if (this.password.length() >= 7 && String.valueOf(this.idCode).length() == 11) {
			return true;
		} else {
			return false;
		}
		
	}
	

}



