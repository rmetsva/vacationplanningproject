/*
-- Query: SELECT * FROM vacation_planning.employees
LIMIT 0, 1000

-- Date: 2017-02-28 23:33
*/
INSERT INTO `employees` (`employee_id`,`name`,`username`,`password`,`id_code`,`department`,`work_beginning_date`,`boss_id`) VALUES (1,'Mati Tamm','matitamm','matitamm1',37004046010,'finance','2014-02-02',NULL);
INSERT INTO `employees` (`employee_id`,`name`,`username`,`password`,`id_code`,`department`,`work_beginning_date`,`boss_id`) VALUES (2,'Tiit Kask','tiitkask','tiitkask1',37103030210,'finance','2015-02-28',1);
INSERT INTO `employees` (`employee_id`,`name`,`username`,`password`,`id_code`,`department`,`work_beginning_date`,`boss_id`) VALUES (3,'Peeter Kuusk','peeterkuusk','peeterkuusk1',37202029010,'finance','2013-02-22',1);
INSERT INTO `employees` (`employee_id`,`name`,`username`,`password`,`id_code`,`department`,`work_beginning_date`,`boss_id`) VALUES (4,'Mari Mänd','marimänd','marimänd1',47502023010,'finance','2017-02-25',1);
