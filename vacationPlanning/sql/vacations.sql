/*
-- Query: SELECT * FROM vacation_planning.vacations
LIMIT 0, 1000

-- Date: 2017-02-28 23:34
*/
INSERT INTO `vacations` (`vacation_id`,`beginning`,`end`,`type`,`status`,`employee_id`) VALUES (1,'2017-04-04','2017-04-15','regular','Rejected',1);
INSERT INTO `vacations` (`vacation_id`,`beginning`,`end`,`type`,`status`,`employee_id`) VALUES (2,'2017-04-04','2017-04-14','regular','Modified',2);
INSERT INTO `vacations` (`vacation_id`,`beginning`,`end`,`type`,`status`,`employee_id`) VALUES (3,'2017-04-04','2017-04-15','regular','Confirmed',3);
